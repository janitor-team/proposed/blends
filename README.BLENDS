This README file explains how to use blend-* scripts.

File system:
- /etc/blends/<Blend>
  Each Blend should create this directory, containing all the infos needed
  by blends-* scripts and by the Blend itself.

  In this way it's clear for users to realize that Blend is using
  Debian Pure Blends framework

- /etc/blends/blends.conf
  Main configuration file of blend-* scripts.
  
  Blends should be aware of it, but not modify it.

- /etc/blends/<Blend>/<Blend>.conf
  A configuration file, in /bin/sh syntax, in which each Blend can
  override /etc/blends/blends.conf

  Blends with particular needs, for example a particular backend, should
  set variables here.
  In this way a default set of parameters is provided by blends.conf and a
  specific set for each Blend instance in <Blend>/<Blend>.conf 

- /usr/share/blends/
  Where common functions for the scripts are stored.
  There will be a directory for each backend and a common set of
  functions that should be mandatory for each registred backend.

- /etc/blends/<Blend>/menu/<ROLE>/
  Users' menus for each registered role.
  If <ROLE> directory is not present, all files in /etc/blends/<Blend>/menu/ 
  are considered to be valid Debian menu entries for any ROLE in Blend.
