<chapter id="existing">
  <title>Existing Debian Pure Blends</title>

  <sect1 id="debian-jr">
  <title>Debian Junior: Debian for children from 1 to 99</title>

<para>
<variablelist>
  <varlistentry>
    <term>Start</term>
     <listitem><para>beginning of 2000</para></listitem>
  </varlistentry>

  <varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://www.debian.org/devel/debian-jr"></ulink></para></listitem>
  </varlistentry>

  <varlistentry>
  <term>Tasks</term>
   <listitem>
    <para>
      <ulink url="http://blends.debian.org/junior/tasks">Tasks of Debian Jr.</ulink>
    </para></listitem>
 </varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-jr/"> debian-jr@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Ben Armstrong <email>synrg@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
    <para>
      <ulink url="http://blends.debian.net/liststats/authorstat_debian-jr.png">Activists on Debian Jr. mailing list</ulink>
    </para></listitem>
</varlistentry>

<varlistentry>
  <term>Release</term>
   <listitem><para>Debian 3.0 (Woody)</para></listitem>
</varlistentry>
  
<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To make Debian an OS that children of all ages will <emphasis>want</emphasis>
           to use, preferring it over the alternatives.</para></listitem>
     <listitem><para>To care for those applications in Debian suitable for children,
           and ensure their quality, to the best of our abilities.</para></listitem>
     <listitem><para>To make Debian a playground for children's enjoyment and
           exploration.</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>
The main target is young children.  By the time children are teenaged, they
should be comfortable with using Debian without any special modifications.
</para>

<para>
Debian Jr. was the first Blend.  In fact, at the time this project was
created, the idea behind of Debian Pure Blends was born, although
then, we used the term "Debian Internal Project".  Over time, this
name was changed to "Custom Debian Distributions" first because it was
too broad, as it was equally descriptive of a number of quite
different projects, such as IPv6 and QA.  The next change of names
became necessary when it was realised that the term "Custom Debian
Distribution" was considered as "something else than Debian" by any
newcomer.  This was so misleading that it effectively blocked a wide
propagation of the principle.
</para>

<para>
Debian Jr. not only provides games, but is also concerned about their
quality from a child's perspective.  Thus, games that are regarded as
not well suited to young children are omitted.  Moreover, choices are
made about which packages are best suited for children to use for
various other activities and tasks that interest them.  This includes,
for example, simple text processing, web browsing and drawing.
</para>

</sect1>


  <sect1 id="debian-med">
  <title>Debian Med: Debian in Health Care</title>

<para>

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-devel/2002/01/msg01730.html">beginning of 2002</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://www.debian.org/devel/debian-med">Debian Med</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem><para><ulink url="http://blends.debian.org/med/tasks">Tasks of Debian Med</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-med/"> debian-med@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Andreas Tille <email>tille@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
     <para><ulink url="http://blends.debian.net/liststats/authorstat_debian-med.png">Activists on Debian Med mailing list</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/authorstat_debian-med-packaging.png">Activists on Debian Med developer list</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/commitstat_debian-med.png">Commiters to Debian Med VCS</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/uploaders_debian-med.png">Uploaders of Debian Med team</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/bugs_debian-med.png">Team members closing the most bugs in Debian Med packages</ulink></para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Release</term>
   <listitem><para>Sarge</para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To build an integrated software environment for all medical tasks.</para></listitem>
     <listitem><para>To care especially for the quality of program packages in the field of medicine 
           that are already integrated within Debian.</para></listitem>
     <listitem><para>To build and include in Debian packages of medical software that are missing
           in Debian.</para></listitem>
     <listitem><para>To care for a general infrastructure for medical users.</para></listitem>
     <listitem><para>To make efforts to increase the quality of third party Free Software
           in the field of medicine.</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

</para>

</sect1>

  <sect1 id="debian-edu">
  <title>Debian Edu: Debian for Education</title>

<para>
<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>Summer of 2002, since 2003 merged with SkoleLinux, which is now
   synonymous with Debian Edu</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://wiki.debian.org/DebianEdu">Debian Edu Wiki</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem><para><ulink url="http://blends.debian.org/edu/tasks">Tasks of Debian Edu</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-edu/"> debian-edu@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
    <para>
      <ulink url="http://blends.debian.net/liststats/authorstat_debian-edu.png">Activists on Debian Edu mailing list</ulink>
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>Responsible</term>
   <listitem><para>Petter Reinholdtsen <email>pere@hungry.com</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Release</term>
   <listitem><para>Sarge</para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To make Debian the best distribution available for educational
           use.</para></listitem> 
     <listitem><para>Provide a ready to run classroom installation with free
           educational software.  An automatically installed server 
           provides net-boot services for disk-less thin clients and
           all necessary applications for educational use.</para></listitem>
     <listitem><para>To federate many initiatives around education, which are
           partly based on forks of Debian.</para></listitem>
     <listitem><para>To continue the internationalisation efforts of SkoleLinux.
           </para></listitem> 
     <listitem><para>To focus on easy installation in schools.</para></listitem>
     <listitem><para>To cooperate with other education-related projects (like 
           <ulink url="http://schoolforge.net/">Schoolforge</ulink>,
           <ulink url="http://www.ofset.org/">Ofset</ulink>, 
           <ulink url="http://edu.kde.org/">KdeEdu</ulink>).</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

This project started with the intention to bring back into Debian a fork from
Debian that was started by some people in France.  Because they had some
time constraints, the people who initially started this effort
handed over responsibility to the Norwegian <ulink url="http://www.skolelinux.org">Skolelinux</ulink>, 
which is currently more or less identical to Debian Edu.
</para>

<para>
The Debian Edu project gathered special interest in Spain because
there are derived Debian distributions from this country that are
intended to be used in schools.  For instance there are: 
  <variablelist>
  <varlistentry>
    <term><ulink url="http://www.linex.org/">LinEX</ulink></term>
     <listitem><para>A Debian derivative distribution used in all schools in
           Extremadura.</para>
           <para>Currently they are joining Debian Edu and by doing so
           becoming fully integrated into Debian.  This is a really
           important move because it brings a lot of good software and
           experience back into Debian.
           </para>
     </listitem>
  </varlistentry>

  </variablelist>
</para>

</sect1>

  <sect1 id="demudi">
  <title>Debian Multimedia</title>

<para>
<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem>
     <para>In 2004 there was and effort by DeMuDi to become a Blend but
         this effort seems to have stalled.  DeMuDi was part of the
	 <ulink url="http://www.agnula.org/">Agnula</ulink> project
         (founded by European Community) and the work somehow was
         taken over by the 64 studio project.
     </para>
     <para>At DebConf 10 in the
        <ulink url="http://penta.debconf.org/dc10_schedule/events/670.en.html">Debian Multimdia BOF</ulink>
        a decision was made to use the Blends stuff for rendering
        web sentinel pages.  It was furtherly mentioned that the
        people driving DeMuDi joined the Debian Multimedia packaging
        team so there is now an unique effort to tackle multimedia
        relevant packages.
     </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para>
    <ulink url="http://wiki.debian.org/DebianMultimedia">Debian Multimedia</ulink></para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem>
    <para><ulink url="http://blends.debian.org/multimedia/tasks">Tasks of Debian Multimedia</ulink>
  </para>
</listitem>

</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
     <para><ulink url="http://blends.debian.net/liststats/authorstat_pkg-multimedia-maintainers.png">Activists on Debian Multimedia maintainers mailing list</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/authorstat_debian-multimedia.png">Activists on Debian Multimedia user mailing list</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/commitstat_pkg-multimedia.png">Commiters to Debian Multimedia VCS</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/uploaders_pkg-multimedia.png">Uploaders of Debian Multimedia team</ulink></para>
     <para><ulink url="http://blends.debian.net/liststats/bugs_pkg-multimedia.png">Team members closing the most bugs in Debian Multimedia packages</ulink></para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Responsible</term>
   <listitem><para>Reinhard Tartler<email>siretart@tauware.de</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>

     <listitem><para>Oriented toward audio and video</para></listitem>
     <listitem><para>To make GNU/Linux a platform of choice for the musician
           and the multimedia artist.</para></listitem>
     <listitem><para>Join multimedia forces inside Debian</para></listitem>

    </itemizedlist>

   </listitem>
</varlistentry>

</variablelist>

</para>
</sect1>

<sect1 id="debian-gis">
  <title>Debian GIS: Geographical Information Systems</title>

<para>

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>
    <ulink url="http://lists.debian.org/debian-devel-announce/2004/10/msg00007.html">October 2004</ulink>
    </para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://wiki.debian.org/DebianGis">DebianGIS Wiki</ulink>
    </para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem>
    <para><ulink url="http://blends.debian.org/gis/tasks">Tasks of Debian GIS</ulink>
    </para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://wiki.debian.org/DebianGis/MailingLists">user and developer list</ulink>
      </para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem><para><ulink url="http://blends.debian.net/liststats/authorstat_debian-gis.png">Activists on Debian GIS mailing list</ulink>
  </para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Francesco P. Lovergine <email>frankie@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>Geographical Information Systems</para></listitem>
     <listitem><para><ulink url="http://www.openstreetmap.org">OpenStreetMap</ulink> and GPS devices</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

</para>

</sect1>

<sect1 id="debichem">
  <title>DebiChem: Debian for Chemistry</title>

<para>

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>October 2004</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://alioth.debian.org/projects/debichem/">Debichem Alioth page</ulink></para>
    </listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem>
    <para><ulink url="http://blends.debian.org/debichem/tasks">Tasks of DebiChem</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para>
    <ulink url="http://lists.alioth.debian.org/mailman/listinfo/debichem-users">debichem-users@lists.alioth.debian.org</ulink>
    </para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
    <para><ulink url="http://blends.debian.net/liststats/authorstat_debichem-devel.png">Activists on DebiChem mailing list</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/commitstat_debichem.png">Commiters to DebiChem VCS</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/uploaders_debichem.png">Uploaders of DebiChem team</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/bugs_debichem.png">Team members closing the most bugs in DebiChem packages</ulink></para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Michael Banck <email>mbanck@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>Chemical applications in Debian</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

</para>

</sect1>

<sect1 id="debian-science">
  <title>Debian Science: Debian for science</title>

<para>
<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-devel/2005/07/msg01555.html">July 2005</ulink>
  </para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://wiki.debian.org/DebianScience">Debian Science Wiki</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem><para><ulink url="http://blends.debian.org/science/tasks">Tasks of Debian Science</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-science/">debian-science@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem>
    <para><ulink url="http://blends.debian.net/liststats/authorstat_debian-science.png">Activists on Debian Science mailing list</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/authorstat_debian-science-maintainers.png">Activists on Debian Science maintainers list</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/commitstat_debian-science.png">Commiters to Debian Science VCS</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/uploaders_debian-science.png">Uploaders of Debian Science team</ulink></para>
    <para><ulink url="http://blends.debian.net/liststats/bugs_debian-science.png">Team members closing the most bugs in Debian Science packages</ulink></para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Responsible</term>
   <listitem><para>Sylvestre Ledru <email>sylvestre@debian.org</email></para></listitem>
</varlistentry>

</variablelist>
</para>

<para>
  While there are Debian Pure Blends that care for certain sciences
  (Debian Med deals in a main part with Biology, DebiChem for
  Chemistry and Debian GIS for geography) not all sciences are covered
  by a specific Blend.  The main reason is that at the moment not
  enough people support such an effort for every science.  The
  temporary solution was to build a general Debian Science Blend that
  makes use of the work of other Blends in case it exists.
</para>
</sect1>

  <sect1 id="accessibility">
  <title>Debian Accessibility Project</title>

<para>
Debian for blind and visually impaired people

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>February 2003</para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-accessibility/">debian-accessibility@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://www.debian.org/devel/debian-accessibility/">Debian Accessibility</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem><para><ulink url="http://blends.debian.org/accessibility/tasks">Tasks of Debian Accessibility</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem><para><ulink url="http://blends.debian.net/liststats/authorstat_debian-accessibility.png">Activists on Debian Accessibility mailing list</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Mario Lang <email>mlang@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To make Debian accessible to people with disabilities.</para></listitem>

     <listitem><para>To take special care for: Screen readers; Screen magnification programs;
	   Software speech synthesisers; Speech recognition software; Scanner drivers
	   and OCR software; Specialised software like edbrowse (web-browse in the
           spirit of line-editors)</para>
     </listitem> 
     <listitem><para>To make text-mode interfaces available.</para></listitem>
     <listitem><para>To provide screen reader functionality during installation.</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

</para>
</sect1>

<sect1 id="ezgo">
  <title>Debian ezgo Project</title>

<para>
 Debian for ezgo project.  ezgo (all small-case) is a project launched by Open Source Software Application Consulting Center
 (OSSACC), which is responsible for promoting free and open source software in Taiwan's schools.  ezgo collects more than one
 hundred FOSS, as well as many public resource tutorials like PhET (http://phet.colorado.edu/), and has a special designed menu
 style.  ezgo aims to introduce the world of FOSS to those who have never heard or touched it.  It is also good for teaching and
 self-learning.  Debian-ezgo is a Debian Pure Blends which integrates ezgo artwork, ezgo menu style, many good FOSS and public
 resource tutorials into debian.

<variablelist>

<varlistentry>
  <term>Start</term>
    <listitem><para>Nov 2009</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
    <listitem><para><ulink url="https://wiki.debian.org/DebianEzGo">Debian ezgo wiki (Outdated)</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
    <listitem><para><ulink url="http://blends.debian.org/ezgo/tasks/">Tasks of Debian ezgo (Outdated)</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
    <listitem><para><ulink url="https://alioth.debian.org/mail/?group_id=100426">debian-ezgo-packaging@lists.alioth.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Responsible</term>
    <listitem><para>Franklin Weng <email>franklin@goodhorse.idv.tw</email></para></listitem>
</varlistentry>

</variablelist>
</para>
</sect1>

<sect1 id="stalled-blends">
  <title>Blends that were announced but development is stalled</title>

  <sect2 id="debian-desktop">
  <title>Debian Desktop: Debian GNU/Linux for everybody</title>

<para>
Motto: "Software that Just Works".

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>October 2002</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://www.debian.org/devel/debian-desktop">Debian Desktop</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-desktop/"> debian-desktop@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
  <listitem><para><ulink url="http://blends.debian.net/liststats/authorstat_debian-desktop.png">Activists on Debian Desktop mailing list</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Colin Walters <email>walters@debian.org</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To try to build the best possible operating system for home
           and corporate workstation use.</para></listitem> 
     <listitem><para>To ensure desktops like GNOME and KDE coexist well in Debian and
           work optimally.</para></listitem>
     <listitem><para>To balance ease of use for beginners with flexibility
           for experts.</para></listitem>
     <listitem><para>To make the system easy to install and configure (e.g. via
           hardware-detection).</para></listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>

This Blend has many common issues with other Blends. The latest move
of Debian Desktop was to care about more up to date software that can
be used as common base for all Debian Pure Blends.  The
common interest is described in detail in <xref linkend="new_ways_of_distribution"/>.  Unfortunately since about 2004 the
project is really silent and it might be considered dead now.
</para>
</sect2>

  <sect2 id="debian-lex">
  <title>Debian Lex: Debian GNU/Linux for Lawyers</title>

<para>
<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>April 2003</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para><ulink url="http://www.debian.org/devel/debian-lex">Debian Lex</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Tasks</term>
   <listitem><para><ulink url="http://blends.debian.org/lex/tasks">Tasks of Debian Lex</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="http://lists.debian.org/debian-lex/"> debian-lex@lists.debian.org</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
   <listitem><para><ulink url="http://blends.debian.net/liststats/authorstat_debian-lex.png">Activists on Debian Lex mailing list</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Jeremy Malcolm <email>Jeremy@Malcolm.id.au</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <listitem><para>To build a complete system for all tasks in legal
           practice.</para></listitem>
     <listitem><para>To add value by providing customised templates for lawyers to
           existing packages like OpenOffice.org and SQL-Ledger, and
	   sample database schemas for PostgreSQL.</para>
     </listitem>
    </itemizedlist>
   </listitem>
</varlistentry>

</variablelist>
The word <emphasis>lex</emphasis> is the Latin word for law.
</para>
</sect2>


  <sect2 id="debian-enterprise">
  <title>Debian Enterprise</title>

<para>
Debian GNU/Linux for Enterprise Computing

<variablelist>

<varlistentry>
  <term>Start</term>
   <listitem><para>End of 2003</para></listitem>
</varlistentry>

<varlistentry>
  <term>URL</term>
   <listitem><para>Debian Enterprise</para></listitem>
</varlistentry>

<varlistentry>
  <term>Initiator</term>
   <listitem><para>Zenaan Harkness <email>zen@iptaustralia.net</email></para></listitem>
</varlistentry>

<varlistentry>
  <term>Activity</term>
  <listitem><para><ulink url="http://blends.debian.net/liststats/authorstat_debian-enterprise.png">Activists on Debian Enterprise mailing list</ulink></para></listitem>
</varlistentry>

<varlistentry>
  <term>Goals</term>
   <listitem>
    <itemizedlist>
     <!-- [BA] The following are stated on the web site as what Debian Enterprise
          does, and not as goals.  But the goals section on the web site is hard
	  to reduce to specific goals point-by-point.  I believe this needs fixing,
	  and perhaps could be improved by talking it over with Zenaan. -->
     <listitem><para>To apply the UserLinux Manifesto.</para></listitem>
     <listitem><para>To establish the benchmark in world class Enterprise
           operating systems engineered within an industry driven
           shared-cost development model.</para></listitem>
     <listitem><para>To vigorously defend its distinctive trademarks and
           branding.</para></listitem>
     <listitem><para>To develop extensive and professional quality
           documentation.</para></listitem>
     <listitem><para>To provide engineer certification through partner
           organisations.</para></listitem>
     <listitem><para>To certify the Debian Enterprise GNU/Linux operating system
           to specific industry standards.</para></listitem>
     <listitem><para>To pre-configure server tasks</para></listitem>
     <!-- [BA] I don't know what this point means, nor can I find it on the
          web site, so perhaps it should simply be removed.
     <listitem>To provide install-time options regarding the intended
           purpose.</listitem>
           [AT] Removed by comment
           The idea behind it was that it might be interesting to
           provide some configuration options for certain servers
           which might have connections to others.  For instance Zope
           can be run behind Apache using some rewriting rules but
           this is poorly documented and a working example or
           something else could help here.  It was discussed via
           E-Mail that Debian Enterprise could provide such
           "inter-package-connection" configurations as examples.
           This was badly worded above and not officially stated at
           their web site, but should not be lost out of focus and
           should remain here at least as comment.  Perhaps you might
           find a better wording.
       -->
    </itemizedlist>

   </listitem>
</varlistentry>

</variablelist>

</para>

</sect2>

  <sect2 id="other">
   <title>Other possible Debian Pure Blends</title>
<para>
  There are fields that could be served nicely by not yet existing
  Blends:  
  <variablelist>

    <varlistentry>
      <term>Debian eGov</term>
       <listitem><para>Could address government issues, administration, offices of authorities,
             accounting.</para></listitem>
    </varlistentry>

    <varlistentry>
      <term>Office</term>
       <listitem><para>Could cover all office issues.</para></listitem>
    </varlistentry>

    <varlistentry>
      <term>Accounting</term>
       <listitem><para>Could integrate accounting systems into Debian.</para></listitem>
    </varlistentry>

    <varlistentry>
      <term>Biology</term>
       <listitem><para>Could perhaps take over some stuff from Debian Med.</para></listitem>
    </varlistentry>

    <varlistentry>
      <term>Physics</term>
       <listitem><para>Might look after simulation software.</para></listitem>
    </varlistentry>

    <varlistentry>
      <term>Mathematics</term>
       <listitem>
        <para>There is even already a live CD - see Quantian in <xref linkend="liveCD"/></para></listitem>
    </varlistentry>

    <varlistentry>
      <term>???</term>
       <listitem><para>There are a lot more potential Blends.</para></listitem>
    </varlistentry>

   </variablelist>
</para>
</sect2>

</sect1>

</chapter>
